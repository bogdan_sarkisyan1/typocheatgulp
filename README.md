# typocheatgulp

How to use :

To launch gulp just type ### `gulp init` 
To add libs you should: 
  1. add css to your libs.scss without .css at the end of the file
  2. add js files to your js-libs-array.js file 
  3. run ### `gulp libs`

If you want to compile libs separately you can just use ### `gulp css-libs` for css and ### `gulp js-libs` for js

When you build your project ( ### `gulp build` ) it will create a folder  inside /public/build