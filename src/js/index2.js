import { exportedJs } from './importedjs';

class Test {
    constructor() {
        this.test = '123';
    }

    showTest() {
        console.log(this.test);
    }
}

const test1 = new Test();

test1.showTest();

console.log(exportedJs + '123123123');
