import gulp from 'gulp';
import sass from 'gulp-sass';
import * as browserSyncWrapper from 'browser-sync';
import autoprefixer from 'gulp-autoprefixer';
import sourcemaps from 'gulp-sourcemaps';
import { path } from './settings';
import { jsInputs } from './jsInputs';
import rollup from '@rollup/stream';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import babel from 'rollup-plugin-babel';
import commonjs from '@rollup/plugin-commonjs';
import nodeResolve from '@rollup/plugin-node-resolve';
import { uglify } from 'rollup-plugin-uglify';
import imgmin from 'gulp-imagemin';
import imgresize from 'gulp-images-resizer';

const browserSync = browserSyncWrapper.create();

function reload(done) {
    browserSync.reload();
    done();
}

const args = (argList => {
    let arg = {},
        a,
        opt,
        thisOpt,
        curOpt;
    for (a = 0; a < argList.length; a++) {
        thisOpt = argList[a].trim();
        opt = thisOpt.replace(/^\-+/, '');

        if (opt === thisOpt) {
            // argument value
            if (curOpt) arg[curOpt] = opt;
            curOpt = null;
        } else {
            // argument name
            curOpt = opt;
            arg[curOpt] = true;
        }
    }

    return arg;
})(process.argv);

gulp.task('sass', function() {
    return gulp
        .src('./src/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: 'compressed' }))
        .pipe(autoprefixer())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.public + '/css'));
});

gulp.task('js', cb => {
    jsInputs.forEach(input => {
        const inputArr = input.split('/');
        const fileName = inputArr[inputArr.length - 1];
        rollup({
            input: input,
            output: {
                sourcemap: true,
                format: 'iife',
                dir: `${path.public}/js`,
            },
            plugins: [
                babel({
                    exclude: 'node_modules/**',
                    presets: ['@babel/preset-env'],
                    runtimeHelpers: true,
                }),
                commonjs(),
                nodeResolve(),
                uglify(),
            ],
        })
            .pipe(source(fileName))

            .pipe(buffer())
            .pipe(sourcemaps.init({ loadMaps: true }))
            .pipe(sourcemaps.write('dist'))
            .pipe(gulp.dest('./public/js'));
    });
    cb();
});

gulp.task('img', cb => {
    gulp.src(`${path.src}/img/**/*`)
        .pipe(
            imgmin(
                [
                    imgmin.mozjpeg({ quality: 75, progressive: true }),
                    imgmin.optipng({ optimizationLevel: 10 }),
                ],
                {
                    verbose: true,
                }
            )
        )
        .pipe(gulp.dest(`${path.public}/img`));

    gulp.src(`${path.src}/img/**/*`)
        .pipe(
            imgmin([
                imgmin.mozjpeg({ quality: 5, progressive: true }),
                imgmin.optipng({ optimizationLevel: 10 }),
            ])
        )
        .pipe(
            imgresize({
                width: 50,
                height: -1,
            })
        )
        .pipe(gulp.dest(`${path.public}/img/thumbs`));

    cb();
});

gulp.task('img-manual', cb => {
    const srcPath = `${path.src}/img/${args.path}${args.path ? '/' : ''}**.*`;
    const destPath = `${path.public}${
        args.thumb
            ? `/img/thumbs${args.path ? '/' + args.path : ''}`
            : `/img${args.path ? '/' + args.path : ''}`
    }`;
    const resize = args['resize'];
    const resizePercent = args['resize-percent'];

    console.log(srcPath);
    gulp.src(srcPath)
        .pipe(
            imgmin(
                [
                    imgmin.mozjpeg({
                        quality: args.quality,
                        progressive: true,
                    }),
                    imgmin.optipng({ optimizationLevel: 10 }),
                ],
                { verbose: true }
            )
        )
        .pipe(
            resize
                ? imgresize({
                      width: resizePercent ? `${resize}%` : parseFloat(resize),
                      height: -1,
                  })
                : gulp.dest(destPath)
        )
        .pipe(gulp.dest(destPath));

    console.log(args);
    console.log(destPath);

    cb();
});

gulp.task(
    'init',
    gulp.series('sass', 'js', function() {
        browserSync.init({
            server: {
                baseDir: path.public,
            },
            notify: false,
        });
        gulp.watch(`${path.src}/scss/**/*.scss`, gulp.series('sass', reload));
        gulp.watch(path.public + '/**/*.html').on('change', browserSync.reload);
        gulp.watch(`${path.src}/js/**/*.js`, gulp.series('js', reload));
    })
);

gulp.task(
    'watch',
    gulp.series('sass', 'js', () => {
        gulp.watch(`${path.src}/scss/**/*.scss`, gulp.series('sass'));
        gulp.watch(`${path.src}/js/**/*.js`, gulp.series('js'));
    })
);

gulp.task('default', gulp.series('init'));
